public class Snake {
	//snake fields
	private String color; 
	private int age;
	private Boolean venomous; 
	
	public Snake(String color, int age, Boolean venomous){
		this.color = color;
		this.age = age;
		this.venomous = venomous;
	}
	
	//set methods to define the values of each field
	public void setAge(int age){
		this.age = age;
	}
	//get methods to use the values of each fields
	public String getColor(){
		return this.color;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public Boolean getVenomous(){
		return this.venomous;
	}
	//determines if the snake can kill you with it's fangs
	public void killHumans() {
		if(this.venomous) {
			System.out.println("Your pet snake has bitten you, vennom is spreading throughout your body.");
		} else {
			System.out.println("Your pet snake is giving you small kisses :)");
		}
	}
	//determine the snake's ability to wiggle
	public void Wiggling() {
		if(this.age > 30) {
			System.out.println("Snake is too old to wiggle....");
		} else {
			System.out.println("Look at him go!");
		}
	}
}