import java.util.Scanner;

public class VirtualPetApp {
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		Snake[] pit = new Snake[1];
		//for loop to construct the object by filling up it's fields
		for(int i = 0; i < pit.length; i++) {
			System.out.println("You adopted a pet snake. What color is he?");
			String colors = reader.nextLine();
			
			System.out.println("How old is he?");
			int ages = Integer.parseInt(reader.nextLine());
			
			System.out.println("Umm is he venomous?");
			Boolean venom = false;
			String isVenomous = reader.nextLine();
			if(isVenomous.equals("yes")) {
				venom = true;
			} else if(isVenomous.equals("no")) {
				venom = false;
			}
			//calls the constructor method to build the object
			pit[i] = new Snake(colors, ages, venom);
			System.out.println(pit[0].getColor());
			System.out.println(pit[0].getAge());
			System.out.println(pit[0].getVenomous());
		}
		System.out.println("Are you sure that's it's age???");
		String choice = reader.nextLine();
		if(choice.equals("no")){
			System.out.println("Enter it's real age");
			int ages = Integer.parseInt(reader.nextLine());
			pit[0].setAge(ages);
			System.out.println(pit[0].getColor());
			System.out.println(pit[0].getAge());
			System.out.println(pit[0].getVenomous());
		}
		else if(choice.equals("yes")){
			System.out.println(pit[0].getColor());
			System.out.println(pit[0].getAge());
			System.out.println(pit[0].getVenomous());
		}
	}
}